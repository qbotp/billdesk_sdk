import hmac
import hashlib
from datetime import datetime
from urllib.request import Request, urlopen
from urllib.parse import urlencode


class BillDesk:

    def __init__(self, merchant_id, security_id, secret_key):
        self.merchant_id = merchant_id
        self.security_id = security_id  # example: kersrtcorp
        self.secret_key = secret_key
        self.type_field1 = 'R'
        self.type_field2 = 'F'
        self.successful_txn_code = '0300'
        self.request_type = '0122'
        self.response_waiting = '0002'
        self.refunded_status = '0699'
        self.initiated_status = '0799'
        self._status_check_url = 'https://www.billdesk.com/pgidsk/PGIQueryController'

    def generate_checksum(self, message):
        '''Generate checksum value.

        Arguments:
            message -- String message to be used for encryption

        Return
            checksum_value -- Encrypted message using the given key
        '''
        key = bytes(self.secret_key, 'utf-8')
        msg = bytes(message, 'utf-8')
        checksum_value = hmac.new(key, msg, hashlib.sha256).hexdigest()

        return checksum_value.upper()

    def get_request_payload(self, customer_id, merchant_txn_ref_no, txn_amount, return_url, info, currency_type='INR'):
        '''Form the request payload to be send to Billdesk

        Arguments:
            customert_id        -- The customer who is making the payment
            merchant_txn_ref_no -- Merchant Transaction Number for the current transaction
            txn_amount          -- Original transaction amount
            return_url          -- Merchant site URL to which user needs to be redirected after payment
            info                -- additional info
            currency_type       -- Currency to be used. Default value is INR

        Return:
            request_payload     -- A Dict containing the payload to Billdesk
        '''
        request_msg = self.__prepare_request_message(str(customer_id), str(merchant_txn_ref_no), str(txn_amount), return_url, info, currency_type)
        request_payload = {'msg': request_msg}

        return request_payload

    def __prepare_request_message(self, customer_id, merchant_txn_ref_no, txn_amount, return_url, info, currency_type):
        '''Prepare Request message for the payload

        Arguments:
            customer_id        -- The customer who is making the payment
            merchant_txn_ref_no -- Merchant Transaction Number for the current transaction
            txn_amount          -- Original transaction amount
            return_url          -- Merchant site URL to which user needs to be redirected after payment
            Info                -- Additional Info
            currency_type       -- Currency to be used

        Return:
            request_msg         -- A string contianing the Request message
        '''
        request_params = [
            str(self.merchant_id),
            str(merchant_txn_ref_no),  # In Billdesk the term used is CustomerID which will be Unique Transaction No
            'NA',
            txn_amount,
            'NA',
            'NA',
            'NA',
            currency_type,
            'NA',
            self.type_field1,
            self.security_id,
            'NA',
            'NA',
            self.type_field2,
            info['domain'],  # request domain
            customer_id,    # user id
            'NA',    # txtAdditional3
            'NA',    # txtAdditional4
            'NA',    # txtAdditional5
            'NA',    # txtAdditional6
            info['corporation_id'],    # txtAdditional7
            return_url
        ]

        request_msg = '|'.join(request_params)
        checksum_generated = self.generate_checksum(request_msg)
        request_msg = request_msg + '|' + checksum_generated

        return request_msg

    def process_bildesk_browser_response(self, response_payload):
        '''Process Billdesk Response after payment redirect

        Arguments:
            response_payload    -- A dict containing the payload from Billdesk

        Return:
            response_key_list   -- A dict with complete details of transaction on success else raise excpetion
        '''
        response_msg = response_payload.get('msg', None).pop()
        param_list = response_msg.split('|')

        if self.__validate_checksum_in_response(response_msg):
            response_params = self.__prepare_response_params(param_list)
            if response_params['auth_status'] == self.successful_txn_code:
                return response_params
            else:
                raise Exception('Payment was not successful', response_params)
        else:
            raise Exception('Response received from unreliable source')

    def __validate_checksum_in_response(self, response_msg):
        param_list_without_checksum = response_msg.split('|')
        checksum = param_list_without_checksum.pop()
        original_msg = '|'.join(param_list_without_checksum)
        generated_checksum = self.generate_checksum(original_msg)

        if checksum == generated_checksum:
            return True

        return False

    def __prepare_response_params(self, param_list):
        '''Prepare response params from the given list

        Arguments:
            response_payload    -- A dict containing the payload from Billdesk

        Return:
            response_key_list   -- A dict with complete details of transaction on success else raise excpetion
        '''
        response_key_list = [
            'merchant_id', 'customer_id', 'txn_ref_no', 'bank_ref_no', 'txn_amount', 'bank_id', 'bank_merchant_id',
            'txn_type', 'currency_name', 'item_code', 'security_type', 'security_id', 'security_password', 'txn_date',
            'auth_status', 'settlement_type', 'additional_info1', 'additional_info2', 'additional_info3',
            'additional_info4', 'additional_info5', 'additional_info6', 'additional_info7', 'error_status',
            'error_description', 'checksum'
        ]
        response_params = dict(zip(response_key_list, param_list))

        return response_params

    def _get_request_payload_for_status(self, customer_id):
        """
        Build request payload for transaction status
        Args:
            customer_id -- This is unique id that was sent in CustomerID in the Payment Request
        Returns:
            dict -- containing request payload
        """
        request_msg = self._prepare_request_message_for_transaction_status(customer_id)
        request_payload = {'msg': request_msg}

        return request_payload

    def _prepare_request_message_for_transaction_status(self, customer_id):
        """
        Prepare request message for transaction status
        Args:
            customer_id  -- This is unique id that was sent in CustomerID in the Payment Request
        Returns:
            string -- request message with checksum
        """
        current_date = format(datetime.now(), '%Y%m%d%H%M%S')
        request_params = [
            self.request_type,
            self.merchant_id,
            str(customer_id),
            current_date
        ]
        request_msg = '|'.join(request_params)
        checksum_generated = self.generate_checksum(request_msg)
        request_msg = request_msg + '|' + checksum_generated

        return request_msg

    def get_transaction_status(self, customer_id):
        """
        Get transaction status
        Args:
            customer_id -- This is unique id that was sent in CustomerID in the Payment Request
        Returns:
            response --  dict holding transaction data
            status   -- True or False
        """
        response = self._build_and_post_request_payload(customer_id)
        if response['auth_status'] == self.successful_txn_code:
            return response, True
        elif response['auth_status'] == self.response_waiting:
            return response, False
        else:
            raise Exception('Transaction Failed')

    def _build_and_post_request_payload(self, customer_id):
        """
        Build and post request payload
        Args:
            customer_id -- This is unique id that was sent in CustomerID in the Payment Request
        Returns:
            dict -- response params
        """
        request_payload = self._get_request_payload_for_status(customer_id)
        request = Request(self._status_check_url, urlencode(request_payload).encode())
        response = urlopen(request).read().decode()
        response = self._build_response_params(response.split('|'))

        return response

    def _build_response_params(self, response):
        """
        Build response params
        Args:
            response -- transaction status response
        Returns:
            dict -- holding transaction data
        """
        response_key_list = [
            'request_type', 'merchant_id', 'customer_id', 'txn_ref_no', 'bank_ref_no', 'txn_amount', 'bank_id',
            'bank_merchant_id', 'txn_type', 'currency_name', 'item_code', 'security_type', 'security_id',
            'security_password', 'txn_date', 'auth_status', 'settlement_type', 'additional_info1',
            'additional_info2', 'additional_info3', 'additional_info4', 'additional_info5', 'additional_info6',
            'additional_info7', 'error_status', 'error_description', 'filter1', 'refund_status',
            'total_refund_amount', 'last_refund_date', 'last_refund_ref_no', 'query_status', 'checksum'
        ]
        response_params = dict(zip(response_key_list, response))

        return response_params

    def get_refund_status(self, customer_id):
        """
        Get refund status against a transaction
        Args:
            customer_id -- This is unique id that was sent in CustomerID in the Payment Request
        Returns:
            bool -- True if refunded otherwise False
        """
        response = self._build_and_post_request_payload(customer_id)
        if response['refund_status'] == self.refunded_status:
            return True
        elif response['refund_status'] == self.initiated_status:
            return False
        else:
            raise Exception('No Refund')
